#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include "boost/date_time/posix_time/posix_time.hpp"
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <iostream>
#include <fstream>
#include <signal.h>
#include <math.h>
using namespace std;
using namespace Eigen;

char* filename;

ofstream outputFile;

string createFileName(){
	boost::posix_time::ptime posix_time = ros::Time::now().toBoost();
	string iso_time_str = boost::posix_time::to_iso_extended_string(posix_time);
	return "authomas_localization_" + iso_time_str + ".csv";;
}

void writePosition(const geometry_msgs::PoseStamped::ConstPtr& msg){

	if (outputFile.is_open()){
		Vector3d euler =  
		Quaterniond(msg->pose.orientation.w, 
					msg->pose.orientation.x, 
					msg->pose.orientation.y, 
					msg->pose.orientation.z).toRotationMatrix().eulerAngles(2, 1, 0);

		outputFile << std::fixed;
		outputFile
		<< msg->header.stamp.toSec() << ","
		<< msg->pose.position.x << ","
		<< msg->pose.position.y << ","
		<< msg->pose.position.z << ","
		<< fmod(euler[0], (2*3.141592)) << ","
		<< fmod(euler[1], (2*3.141592)) << ","
		<< fmod(euler[2], (2*3.141592)) << endl;


	} else{
		ROS_ERROR_STREAM("Unable to open output file " << filename);
	}
}

void closeFileSignitHandler(int sig){
	if (outputFile.is_open()){
		ROS_INFO_STREAM("Closing output file " << filename);
		outputFile.close();
	}
	ros::shutdown();
}

int main(int argc, char **argv){
	ros::init(argc, argv, "csv_node", ros::init_options::NoSigintHandler);
	ros::NodeHandle n;
	ros::Time::init();
	string filename_string = createFileName();
	filename = &filename_string[0];
	outputFile = ofstream(filename);
	ros::Subscriber pose_subscriber = n.subscribe("/car_position", 1000, writePosition);

	signal(SIGINT, closeFileSignitHandler);
	ros::spin();
	return 0;
}