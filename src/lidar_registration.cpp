#include <string>
#include <ros/ros.h>
#include "sensor_msgs/PointCloud2.h"
#include "geometry_msgs/PointStamped.h"
#include <Eigen/Dense>
#include <math.h>
#include <eigen_conversions/eigen_msg.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/registration/ndt.h>
#include <pcl/filters/approximate_voxel_grid.h>

using namespace Eigen;

const char * MAP_PATH = "map.pcd";
pcl::PointCloud<pcl::PointXYZ>::Ptr map_cloud (new pcl::PointCloud<pcl::PointXYZ>);

ros::Publisher ndt_pub;

// Initializing Normal Distributions Transform (NDT).
pcl::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ> ndt;


AngleAxisf last_rot(0, Vector3f::UnitZ());
Translation3f last_pos(-1,-1,-1);
bool initial_gps_pos_received = false;

Affine3f compute_NDT(const boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZ> >& obs_cloud, Eigen::Translation3f last_translation, Eigen::AngleAxisf last_rotation)
{
  std::cout << "computing NDT..\n";
  // Filtering input scan to roughly 10% of original size to increase speed of registration.
  pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cloud (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::ApproximateVoxelGrid<pcl::PointXYZ> approximate_voxel_filter;
  approximate_voxel_filter.setLeafSize (0.2, 0.2, 0.2);
  approximate_voxel_filter.setInputCloud (obs_cloud);
  approximate_voxel_filter.filter (*filtered_cloud);
  std::cout << "Filtered cloud contains " << filtered_cloud->size ()
            << " data points from observation" << std::endl;


  // Setting point cloud to be aligned.
  ndt.setInputSource (filtered_cloud);
  // Set initial alignment estimate found using robot odometry.
  Eigen::Matrix4f init_guess = (last_translation * last_rotation).matrix ();

  // Calculating required rigid transform to align the input cloud to the target cloud.
  pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud (new pcl::PointCloud<pcl::PointXYZ>);

  ros::WallTime t1 = ros::WallTime::now();
  ndt.align (*output_cloud, init_guess);
  ros::WallTime t2 = ros::WallTime::now();

  std::cout << "time : " << (t2 - t1).toSec() * 1000 << "[msec]" << std::endl;

  std::cout << "Normal Distributions Transform has converged:" << ndt.hasConverged ()
            << " score: " << ndt.getFitnessScore () << " - number of iterations: " << ndt.getFinalNumIteration() << std::endl;

  Affine3f transformation;
  transformation.matrix() = ndt.getFinalTransformation();
  last_rot = AngleAxisf(transformation.rotation());
  last_pos = Translation3f(transformation.translation());
}

void gps_callback(const boost::shared_ptr<const geometry_msgs::PointStamped>& msg)
{
  if(initial_gps_pos_received)
    return;
  last_pos = Translation3f(msg->point.x, msg->point.y, msg->point.z);
  //last_pos = Translation3f(-263.5926208496094f,-67.85790252685547f,-9.890708923339844f);

  Eigen::AngleAxisf yawAngle(-2.199310397111461f, Eigen::Vector3f::UnitZ());
  Eigen::AngleAxisf pitchAngle(-0.038709807735220494f, Eigen::Vector3f::UnitY());
  Eigen::AngleAxisf rollAngle(0.024419771060208238f, Eigen::Vector3f::UnitX());

  //last_rot = rollAngle * yawAngle * pitchAngle;

  initial_gps_pos_received = true;
  std::cout << "received gps\n";
}

void lidar_callback(const boost::shared_ptr<const sensor_msgs::PointCloud2>& msg)
{
  if (!initial_gps_pos_received)
    return;
  pcl::PCLPointCloud2 pcl_pc2;
  pcl_conversions::toPCL(*msg, pcl_pc2);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::fromPCLPointCloud2(pcl_pc2,*cloud);
  compute_NDT(cloud, last_pos, last_rot);
  //ndt_pub.publish(ndt);
  Vector3f yaw_pitch_roll = last_rot.toRotationMatrix().eulerAngles(2, 1, 0);
  std::cout << "New position: x: " << last_pos.x() << " y: " << last_pos.y() << " z: " << last_pos.z() << std::endl;
  std::cout << "New rotation: yaw: " << yaw_pitch_roll[0] << " pitch: " << yaw_pitch_roll[1] << " roll: " << yaw_pitch_roll[2] << std::endl;
}

void init_ntd()
{
  // Setting scale dependent NDT parameters
  // Setting minimum transformation difference for termination condition.
  ndt.setTransformationEpsilon (0.01);
  // Setting maximum step size for More-Thuente line search.
  ndt.setStepSize (0.1);
  //Setting Resolution of NDT grid structure (VoxelGridCovariance).
  ndt.setResolution (1.0);

  // Setting max number of registration iterations.
  ndt.setMaximumIterations (100);
  // Setting point cloud to be aligned to.
  ndt.setInputTarget (map_cloud);

  std::cout << "initialized NDT\n";
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "lidar_registration");

  pcl::PointCloud<pcl::PointXYZ>::Ptr unfiltered_map (new pcl::PointCloud<pcl::PointXYZ>);
  if (pcl::io::loadPCDFile<pcl::PointXYZ> (MAP_PATH, *unfiltered_map) == -1)
  {
    PCL_ERROR ("Couldn't read file \n");
    return (-1);
  }
  std::cout << "Loaded " << unfiltered_map->size () << " data points from map file" << std::endl;

  pcl::ApproximateVoxelGrid<pcl::PointXYZ> approximate_voxel_filter;
  approximate_voxel_filter.setLeafSize (1.0, 1.0, 1.0);
  approximate_voxel_filter.setInputCloud (unfiltered_map);
  approximate_voxel_filter.filter (*map_cloud);
  std::cout << "Filtered cloud contains " << map_cloud->size ()
            << " data points from map" << std::endl;


  ros::NodeHandle n;

  ros::Subscriber lidar_sub = n.subscribe("/lidar_points", 1000, lidar_callback);
  ros::Subscriber gps_sub = n.subscribe("/fix", 1000, gps_callback);

  init_ntd();

  ros::spin();
  return 0;
}