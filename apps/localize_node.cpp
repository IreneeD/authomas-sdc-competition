#include <iostream>
#include <ros/ros.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/registration/ndt.h>
#include <pcl/registration/gicp.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl_conversions/pcl_conversions.h>
#include <eigen_conversions/eigen_msg.h>

#include <pclomp/ndt_omp.h>
#include <pclomp/gicp_omp.h>

#include "sensor_msgs/PointCloud2.h"
#include "sensor_msgs/Imu.h"
#include "geometry_msgs/PointStamped.h"
#include "geometry_msgs/PoseStamped.h"
#include "tf2_msgs/TFMessage.h"
#include "geometry_msgs/TransformStamped.h"
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>


#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include "tf/transform_datatypes.h"
#include "tf_conversions/tf_eigen.h"

#include <message_filters/time_synchronizer.h>
#include <message_filters/subscriber.h>

#include <signal.h>

using namespace Eigen;

const char * MAP_PATH = "map.pcd";
const float LEAF_SIZE = 0.3;
const float LEAF_SIZE_MAP = 0.3;

enum algorithm {
  GICP,
  NDT_OMP,
  NDT,
  ICP
};

algorithm USED_ALGO = NDT_OMP;

int num_threads;
const bool ONLY_USE_INITIAL_GUESS_ONCE = false;
bool initial_gps_received = false;
bool initial_imu_received = false;

Affine3d tf_eigen = Eigen::Affine3d::Identity();
tf::TransformListener * listener_;

const int METHOD_INDEX = 1;
std::vector<std::pair<std::string, pclomp::NeighborSearchMethod>> search_methods = {
  {"KDTREE", pclomp::KDTREE},
  {"DIRECT7", pclomp::DIRECT7},
  {"DIRECT1", pclomp::DIRECT1}
};

geometry_msgs::TransformStamped lidar_tf;

pcl::PointCloud<pcl::PointXYZ>::Ptr map_cloud (new pcl::PointCloud<pcl::PointXYZ>);

pclomp::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ> ndt_omp;
pcl::GeneralizedIterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> gicp;
pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
pcl::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ> ndt;

ros::Publisher pose_pub;
geometry_msgs::PoseStamped pose_msg;

Quaternionf last_rot;
Quaternionf last_imu;
Vector3f last_pos(-1,-1,-1);
Vector3f last_gps(-1,-1,-1);
Matrix4f last_mat;

pcl::VoxelGrid<pcl::PointXYZ> voxelgrid;

int timestep_count = 0;

// align point clouds and measure processing time
void align(pcl::Registration<pcl::PointXYZ, pcl::PointXYZ> & registration, const pcl::PointCloud<pcl::PointXYZ>::Ptr& target_cloud, const pcl::PointCloud<pcl::PointXYZ>::Ptr& source_cloud ) {
  registration.setInputSource(source_cloud);
  pcl::PointCloud<pcl::PointXYZ>::Ptr aligned(new pcl::PointCloud<pcl::PointXYZ>());

  Eigen::Matrix4f init_guess = Eigen::Matrix4f::Identity();

  if(!ONLY_USE_INITIAL_GUESS_ONCE || !initial_gps_received)
  {
    init_guess.block<3, 3>(0, 0) = last_imu.toRotationMatrix();
    init_guess.block<3, 1>(0, 3) = last_gps;
    initial_gps_received = true;
  }
  else
  {
    init_guess.block<3, 3>(0, 0) = last_imu.toRotationMatrix();
    init_guess.block<3, 1>(0, 3) = last_pos;
  }
  auto t1 = ros::WallTime::now();
  registration.align(*aligned, init_guess);
  auto t2 = ros::WallTime::now();
  std::cout << "timestep:" << timestep_count << std::endl;
  std::cout << "time : " << (t2 - t1).toSec() * 1000 << "[msec]" << std::endl;
  std::cout << "converged : " << registration.hasConverged() << std::endl;
  std::cout << "fitness: " << registration.getFitnessScore() << std::endl << std::endl;

//TODO
  last_mat = registration.getFinalTransformation();
  Vector3f pos = last_mat.block<3, 1>(0, 3);
  Quaternionf rot(last_mat.block<3, 3>(0, 0));

  if(last_rot.coeffs().dot(rot.coeffs()) < 0.0f) {
    rot.coeffs() *= -1.0f;
  }
  last_rot = rot;
  last_pos = pos;
  timestep_count ++;
}

void imu_callback(const boost::shared_ptr<const sensor_msgs::Imu>& imu)
{
  //if(ONLY_USE_INITIAL_GUESS_ONCE && initial_imu_received)
  //  return;
  last_imu = Quaternionf(imu->orientation.w, imu->orientation.x, imu->orientation.y, -imu->orientation.z);
  last_imu = AngleAxisf(3.1415926, Vector3f::UnitY()) * AngleAxisf(3.1415926, Vector3f::UnitX()) * last_imu;
  initial_imu_received = true;
}

void tf_callback(const boost::shared_ptr<const tf2_msgs::TFMessage>& tf)
{
  lidar_tf = tf->transforms[0];
  //std::cout << lidar_tf.child_frame_id << std::endl;
  //std::cout << "tf translation x: " << lidar_tf.transform.translation.x << " y: " << lidar_tf.transform.translation.y << " z: " << lidar_tf.transform.translation.z << std::endl;
  //std::cout << "tf rotation w: " << lidar_tf.transform.rotation.w << " x: " << lidar_tf.transform.rotation.x << " y: " << lidar_tf.transform.rotation.y << " z: "  << lidar_tf.transform.rotation.z << std::endl;
}

void callback(const boost::shared_ptr<const sensor_msgs::PointCloud2>& msg, const boost::shared_ptr<const geometry_msgs::PointStamped>& gps)
{
  last_gps = Vector3f(gps->point.x, gps->point.y, gps->point.z);

  pcl::PCLPointCloud2 pcl_pc2;
  pcl_conversions::toPCL(*msg, pcl_pc2);
  pcl::PointCloud<pcl::PointXYZ>::Ptr obs_cloud(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::fromPCLPointCloud2(pcl_pc2,*obs_cloud);

  // downsample observation cloud
  
  pcl::PointCloud<pcl::PointXYZ>::Ptr downsampled(new pcl::PointCloud<pcl::PointXYZ>());

  voxelgrid.setInputCloud(obs_cloud);
  voxelgrid.filter(*downsampled);
  *obs_cloud = *downsampled;
  
  tf::StampedTransform tf_stamped;
  geometry_msgs::TransformStamped tf_msg;
  listener_->lookupTransform("/base_link", "/velodyne", ros::Time(0), tf_stamped);
  tf::transformStampedTFToMsg(tf_stamped, tf_msg);
  Eigen::Affine3d tf_eigen = Eigen::Affine3d::Identity();
  tf::transformMsgToEigen(tf_msg.transform, tf_eigen);
  pcl::transformPointCloud (*obs_cloud, *obs_cloud, tf_eigen);

  //std::cout << "--- pclomp::NDT (" << search_methods[METHOD_INDEX].first << ", " << num_threads << " threads) ---" << std::endl;
  if (USED_ALGO == NDT){
    align(ndt, map_cloud, obs_cloud);
  }
  else if(USED_ALGO == NDT_OMP){
    align(ndt_omp, map_cloud, obs_cloud);
  }
  else if (USED_ALGO == ICP){
    align(icp, map_cloud, obs_cloud);
  }
  else if (USED_ALGO == GICP){
    align(gicp, map_cloud, obs_cloud);
  }
  else{
    std::cout << "ERROR, NO ALGO PICKED" << std::endl;
  }

  //ndt_pub.publish(ndt);
  Vector3f yaw_pitch_roll = last_rot.toRotationMatrix().eulerAngles(2, 1, 0);
  std::cout << "New position: x: " << last_pos.x() << " y: " << last_pos.y() << " z: " << last_pos.z() << std::endl;
  std::cout << "New rotation: yaw: " << yaw_pitch_roll[0] << " pitch: " << yaw_pitch_roll[1] << " roll: " << yaw_pitch_roll[2] << std::endl;
  

  pose_msg.header.stamp = msg->header.stamp;
  pose_msg.header.seq = msg->header.seq;
  pose_msg.header.frame_id = "pose";

  pose_msg.pose.position.x = last_pos.x();
  pose_msg.pose.position.z = last_pos.z();
  pose_msg.pose.position.y = last_pos.y();

  pose_msg.pose.orientation.x = last_rot.x();
  pose_msg.pose.orientation.y = last_rot.y();
  pose_msg.pose.orientation.z = last_rot.z();
  pose_msg.pose.orientation.w = last_rot.w();


  //tf2::doTransform(pose_msg.pose, pose_msg.pose, lidar_tf);
  std::cout << "TF position: x: " << pose_msg.pose.position.x << " y: " << pose_msg.pose.position.y << " z: " << pose_msg.pose.position.z << std::endl;
  std::cout << "TF orientation: x: " << pose_msg.pose.orientation.x << " y: " << pose_msg.pose.orientation.y << " z: " << pose_msg.pose.orientation.z << " w: " << pose_msg.pose.orientation.w << std::endl;

  pose_pub.publish(pose_msg);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "lidar_registration");

  pcl::console::setVerbosityLevel(pcl::console::L_DEBUG);

  // reading map file
  if (pcl::io::loadPCDFile<pcl::PointXYZ> (MAP_PATH, *map_cloud) == -1)
  {
    PCL_ERROR ("Couldn't read file \n");
    return (-1);
  }
  std::cout << "Loaded " << map_cloud->size () << " data points from map file" << std::endl;

  // downsampling
/*
  pcl::PointCloud<pcl::PointXYZ>::Ptr downsampled2(new pcl::PointCloud<pcl::PointXYZ>());
  pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
  sor.setInputCloud (map_cloud);
  sor.setMeanK (50);
  sor.setStddevMulThresh (1.0);
  sor.filter (*downsampled2);
  *map_cloud = *downsampled2;

  std::cout << "Filtered map to " << map_cloud->size () << " data points (stats)" << std::endl;
*/
  pcl::PointCloud<pcl::PointXYZ>::Ptr downsampled(new pcl::PointCloud<pcl::PointXYZ>());
  voxelgrid.setLeafSize(LEAF_SIZE_MAP, LEAF_SIZE_MAP, LEAF_SIZE_MAP);

  voxelgrid.setInputCloud(map_cloud);
  voxelgrid.filter(*downsampled);
  *map_cloud = *downsampled;

  std::cout << "Filtered map to " << map_cloud->size () << " data points (voxel)" << std::endl;
  
  num_threads = omp_get_max_threads();

  // initialize orientation
  last_rot = AngleAxisf(0, Vector3f::UnitZ());

  if (USED_ALGO == NDT_OMP){
    ndt_omp.setResolution(1.0);
    ndt_omp.setNumThreads(num_threads);
    ndt_omp.setNeighborhoodSearchMethod(search_methods[METHOD_INDEX].second);
    ndt_omp.setTransformationEpsilon(1e-8);
    ndt_omp.setStepSize(0.1);
    ndt_omp.setMaximumIterations(100);
    ndt_omp.setInputTarget(map_cloud);
  }
  else if (USED_ALGO == NDT){
    ndt.setTransformationEpsilon(0.001);
    ndt.setStepSize(0.1);
    ndt.setMaximumIterations(100);
    ndt.setResolution(1.0);
    ndt.setInputTarget(map_cloud);
  }
  else if (USED_ALGO == ICP){
    icp.setInputTarget(map_cloud);
    icp.setMaxCorrespondenceDistance (1);
    icp.setMaximumIterations (100);
    icp.setTransformationEpsilon (1e-10);
    icp.setEuclideanFitnessEpsilon (0.0001);
  }

  // subscribers
  ros::NodeHandle n;

  listener_ = new tf::TransformListener;
  

  message_filters::Subscriber<sensor_msgs::PointCloud2> lidar_sub(n, "/lidar_points", 1000);
  message_filters::Subscriber<geometry_msgs::PointStamped> gps_sub(n, "/fix", 1000);
  ros::Subscriber tf_sub = n.subscribe("/tf", 1000, tf_callback);
  ros::Subscriber imu_sub = n.subscribe("/imu/data", 1000, imu_callback);
  message_filters::TimeSynchronizer<sensor_msgs::PointCloud2, geometry_msgs::PointStamped> sync(lidar_sub, gps_sub, 1000);
  sync.registerCallback(boost::bind(&callback, _1, _2));

  pose_pub = n.advertise<geometry_msgs::PoseStamped>("/car_position", 1000);

  std::cout << "ready!" << std::endl;

  ros::spin();
  return 0;
}